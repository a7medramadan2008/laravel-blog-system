<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Romio') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


<nav class="navbar navbar-default" role="navigation">
 <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="/">Romio System</a>
          </div>
     
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav">
                    
                    <li><a href="/Admin">Posts</a></li>
                    <li><a href="/Admin/create"><i class="fas fa-plus"></i> Write Post</a></li>
                    <li><a href="{{ route('home.Admin') }}"><i class="fas fa-cog"></i> Admin</a></li>
                    
                    
                    <li class="btn btn-info btn-sm pull-right">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" ">
                                    {{ csrf_field() }}
                                    
                     <input type="submit" value="logout" name="submit" class="btn btn-info btn-sm pull-right ">
                     </form></li>
                     
                    
               </ul>
                &nbsp  &nbsp<li class="btn btn-info btn-xl ">Admin Mode</li>
                </li>
               </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">



                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                
                         <ul class="dropdown-menu">
                            <li>
                                <a href="/Admin/create">
                                    <i class="fas fa-plus"></i> Write Post
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('home') }}">
                                    <i class="fas fa-cog"></i> Admin
                                </a>
                            </li>

                            <li class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    <input type="submit" value="logout" name="submit">
                                </form>
                            </li>
                        </ul>
                            </li>
                        @endguest
                    </ul>

          </div><!-- /.navbar-collapse -->
          </div>
     </nav>




     