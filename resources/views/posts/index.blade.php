@extends('layouts.default')

@section('content')
{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
@if ((session('error')))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input1.<br><br>
    <ul>

        <li>{{ session('error') }}</li>

    </ul>
</div>
@endif --}}
{{-- <a href="{{ url('downloadExcel/xls') }}"><button class="btn btn-success">Download Excel xls</button></a> --}}
<div class="form-group">
    <a href="{{ url('/') }}/export/xlsx" class="btn btn-success">Export to .xlsx</a>
    <a href="{{ url('/') }}/export/book.xls" class="btn btn-primary">Export to .xls</a>
    <br>
    <form action="{{route('posts.search')}}" method="post" class="form-inline">
        @csrf
        <div class="form-group">
            <label>FROM</label>
            <input type="date" name="from" value="" class="form-control">

            <label>TO</label>
            <input type="date" name="to" value="" class="form-control">

            <div class="form-group">
                <button type="submit"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>

</div>
@if($posts->count()>0)
@foreach($posts as $post)
<div class="panel">

    <div class="panel-heading">
        <h3>
            <a href=" posts/{{$post->slug}}  ">
                {{ $post->title }}

            </a>
        </h3>

    </div>

    <div class="panel-body">

        {{Str::limit(strip_tags($post->body),20) }}

    </div>

    <div class="panel-footer">
        <span class="label label-info">
            <i class="fas fa-calendar"></i> {{ $post->created_at }}
        </span>

        &nbsp
        <span class="label label-primary">
            <i class="fas fa-user"></i> {{ $post->user['name'] }}
        </span>


    </div>



</div>
@endforeach

{{ $posts->links() }}
@else
<div class="alert alert-info">
    <strong>Ops</strong>No posts
</div>
@endif

@endsection
