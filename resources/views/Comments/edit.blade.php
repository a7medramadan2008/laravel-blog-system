@extends('layouts.Admin_layout')

@section('content1')

<h1>Edit Comment </h1>
<hr />

{{ Form::open(['action'=> ['CommentsController@update', $comment1->id ], 'method'=>'POST', 'files'=>true ])  }}

{{ Form::hidden ('_method' ,'PUT')}}

<div class="form-group">
    {{ Form::label('Body') }}
    {{ Form::textarea('body', $comment1->body , [ 'placeholder'=>'Enter Post Body', 'class'=>'form-control ckeditor' ]) }}
</div>

<div class="form-group pull-right">
    {{ Form::submit('Update', ['class'=>'btn btn-primary']) }}
</div>
{{ Form::close() }}

@endsection
