@extends('layouts.default')'

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="btn-group pull-right">
                    <a href="posts/create" class="btn btn-default btn-xs">
                        <i class="fas fa-plus"></i> New Post
                    </a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    You are logged in!

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Created</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                            <tr>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->created_at }}</td>

                                <td>
                                    <a href="/posts/{{ $post->id }}/edit" class="btn btn-info btn-xs">
                                        <i class='fas fa-edit'></i> Edit Post
                                    </a>
                                </td>

                                <td>
                                    <div class="pull-right">
                                        {{ Form::open(['action'=> ['postscontroller@destroy',$post->id], 'method'=>'POST' ])  }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button class="btn btn-danger btn-xs" type="submit"> Delete Post </button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
