<!DOCTYPE html>
<html>
<head>
 <!-- CSRF Token -->
 <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{config('app.name'),'Romoi Blog'}}</title>
     <link rel="stylesheet" type="text/css" href="{{ asset('app.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ asset('extra.css') }}">
     <script src="//cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
     <script defer src="https://use.fontawesome.com/releases/v5.0.1/js/all.js"></script>
</head>
<body>
	
     @include('elements.navbar')
          
     <div class="container">
     @include('elements.flash')
          @yield('content')
     </div>
<script src="{{asset('app.js')}}"></script>


</body>
</html>