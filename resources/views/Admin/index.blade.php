@extends('layouts.Admin_layout')

@section('content1')
<h1> Admin panel</h1>
<hr />
<a href="{{ url('downloadExcel/book.xls') }}"><button class="btn btn-success">Download Excel xls</button></a>
@if($posts->count()>0)
@foreach($posts as $post)
<div class="panel">

    <div class="panel-heading">
        <h3>
            <a href=" Admin/{{$post->slug}}  ">
                {{ $post->title }}

            </a>
        </h3>
    </div>

    <div class="panel-body">
        {{Str::limit(strip_tags($post->body),20) }}
    </div>

    <div class="panel-footer">
        <span class="label label-info">
            <i class="fas fa-calendar"></i> {{ $post->created_at }}
        </span>

        &nbsp
        <span class="label label-primary">
            <i class="fas fa-user"></i> {{ $post->user['name'] }}
        </span>
    </div>


</div>
@endforeach

{{ $posts->links() }}
@else
<div class="alert alert-info">
    <strong>Ops</strong>No posts
</div>
@endif

@endsection
