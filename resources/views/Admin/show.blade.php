@extends('layouts.Admin_layout')

@section('content1')
<h1> Admin Mode</h1>
<hr />
<h1> {{$post->title}} </h1>

<div class="clear-fix">
    <a href=" /Admin/{{ $post->id }}/edit " class="btn btn-default">Edit Post</a>
    <div class="pull-right">
        {{ Form::open(['action'=> ['Admincontroller@destroy',$post->id], 'method'=>'POST' ])  }}
        {{ Form::hidden('_method', 'DELETE') }}
        <button class="btn btn-danger" type="submit"> Delete Post </button>
        {!! Form::close() !!}
    </div>
</div>

<hr />
<div>
    {!! $post->body !!}
</div>

<h4>Comments: {{ $post->comments->count() }}</h4>

<!-- Comments List -->
<ul class="comments">

    @foreach($post->comments as $comment)
    <li class="comment">
        <div class="clearfix">
            <h4 class="pull-left">{{ $comment->user->name }}</h4>
            <p class="pull-right">{{ $comment->created_at->format('d M Y') }}</p>

        </div>

        <p>{!! $comment->body !!}</p>

        <!-- Edit and delet button for comments in admin mode -->
        <div class="clear-fix">
            <a href="{{route('editComment',$comment->id)}}" class="btn btn-default">Edit comment</a>
            <div class="pull-right">
                {{ Form::open(['action'=> ['CommentsController@destroy',$post->id], 'method'=>'POST' ])  }}
                {{ Form::hidden('_method', 'DELETE') }}
                <button class="btn btn-danger" type="submit"> Delete comment </button>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- Edit and delet button for comments in admin mode -->
    </li>
    @endforeach
</ul>
<!-- Comments List -->


<!-- Comments Form -->
{{-- <div class="panel panel-default > --}}
<div class="panel-heading">Add Your Comment</div>
<div class="panel-body">

    @guest
    <div class="alert alert-info">Please login to comment</div>
    @else
    <form action="{{ route('comments.store', $post->slug) }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="Comment">Comment</label>
            <textarea name="body" class="form-control" placeholder="Enter your comment" cols="30" rows="10"></textarea>
        </div>

        <div class="form-group text-right">
            <button type="submit" class="btn btn-primary">Add Comment</button>
        </div>
    </form>
    @endguest

</div>
</div>
<!-- Comments Form -->

@endsection
