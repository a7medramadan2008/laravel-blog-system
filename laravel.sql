-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2019 at 10:24 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 3, 19, 'this is my frist comment', '2019-11-02 04:53:10', '2019-11-02 04:53:10'),
(2, 3, 19, 'my second comment ....grate job', '2019-11-02 04:54:52', '2019-11-02 04:54:52'),
(3, 3, 19, 'this my third comment', '2019-11-02 04:58:02', '2019-11-02 04:58:02'),
(4, 1, 1, 'frist comment', '2019-11-02 05:22:25', '2019-11-02 05:22:25'),
(5, 1, 1, 'second comment', '2019-11-02 05:22:49', '2019-11-02 05:22:49'),
(6, 3, 2, 'commented by maher', '2019-11-02 05:32:05', '2019-11-02 05:32:05'),
(7, 3, 1, 'commented by maher', '2019-11-02 05:32:45', '2019-11-02 05:32:45'),
(8, 3, 2, 'test comment for editing', '2019-11-02 09:07:12', '2019-11-02 09:07:12'),
(18, 2, 6, 'i\'m proud of my self', '2019-11-03 06:12:27', '2019-11-03 06:12:27'),
(19, 2, 4, 'wellcome', '2019-11-03 06:29:12', '2019-11-03 06:29:12'),
(21, 2, 5, 'nice comment', '2019-11-03 06:32:17', '2019-11-03 06:32:17'),
(22, 2, 7, 'sure sir', '2019-11-03 06:44:57', '2019-11-03 06:44:57'),
(23, 2, 7, 'greate job', '2019-11-03 06:45:15', '2019-11-03 06:45:15'),
(25, 2, 8, 'graet job nice work', '2019-11-03 06:47:41', '2019-11-03 06:47:41'),
(26, 2, 5, 'grate job nice work', '2019-11-03 06:48:15', '2019-11-03 06:48:15'),
(27, 2, 5, 'it is ok', '2019-11-03 06:48:28', '2019-11-03 06:48:28'),
(28, 2, 5, 'nice work', '2019-11-03 06:51:13', '2019-11-03 06:51:13');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(21, '2014_10_12_000000_create_users_table', 1),
(22, '2014_10_12_100000_create_password_resets_table', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2019_10_29_085716_create_posts_table', 1),
(25, '2019_10_30_123119_add_slug_topost', 1),
(26, '2019_10_30_205115_add_user_id_to_post', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`, `slug`, `user_id`) VALUES
(4, 'maher post1', '<p>maher posts1</p>', '2019-11-02 09:47:44', '2019-11-02 11:48:03', 'maher-post-20191102111144', 3),
(5, 'post after Admin panel ceration', '<p>post after Admin panel cerationpost after Admin panel cerationpost after Admin panel cerationpost after Admin panel ceration1</p>', '2019-11-03 06:09:00', '2019-11-03 06:37:31', 'post-after-admin-panel-ceration-20191103081100', 1),
(7, 'nice blog', '<p>nice blognice blognice blognice blognice blog</p>', '2019-11-03 06:44:36', '2019-11-03 06:44:36', 'nice-blog-20191103081136', 2),
(8, 'frist post', '<p>frist postfrist postfrist postfrist postfrist postfrist postfrist postfrist postfrist postfrist postfrist postfrist post</p>', '2019-11-03 06:46:53', '2019-11-03 06:46:53', 'frist-post-20191103081153', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed Ramadan', 'a7medramadan2008@gmail.com', NULL, '$2y$10$wEXKL6y5cBwKXhm8Va2CTufY27EGmHnLJlxbBqd2mN2P/zCY4K4va', NULL, '2019-11-02 05:13:32', '2019-11-02 05:13:32'),
(2, 'Ahmed Maher', 'a7medmaher2008@gmail.com', NULL, '$2y$10$MIhdjMjQ0Ze2MgYqUMIcOuiM2AGXuWV24FMKQepNam5sjGzIfYho2', NULL, '2019-11-02 05:26:46', '2019-11-02 05:26:46'),
(3, 'Amed Maher', 'ahmedmaher2008@gmail.com', NULL, '$2y$10$76GGBar3fPx1mxPl8dybBu5AQTz0wkYGT3T2PDrr/VvtjVGKgHbla', NULL, '2019-11-02 05:28:17', '2019-11-02 05:28:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
