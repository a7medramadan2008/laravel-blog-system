<?php

namespace App\Http\Controllers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\post;

class EmpController extends Controller
{



    public function export($fileName)
    {
        $employees = post::all();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Id');
        $sheet->setCellValue('B1', 'title');
        $sheet->setCellValue('C1', 'body');

        $rows = 2;
        foreach ($employees as $empDetails) {
            $sheet->setCellValue('A' . $rows, $empDetails['id']);
            $sheet->setCellValue('B' . $rows, $empDetails['title']);
            $sheet->setCellValue('C' . $rows, $empDetails['body']);

            $rows++;
        }

        return response()->streamDownload(function () use($spreadsheet, $fileName) {
            $class = '\\PhpOffice\\PhpSpreadsheet\\Writer\\';
            $class.=  substr($fileName, -4) === 'xlsx' ? 'Xlsx' : 'Xls';
            $writer = new $class($spreadsheet);
            $writer->save('php://output');
        }, $fileName, [
            'Content-Type' => 'application/vnd.ms-excel'
        ]);
    }
}
