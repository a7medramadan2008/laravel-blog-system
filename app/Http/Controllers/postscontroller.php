<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\post;

class postscontroller extends Controller
{



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        //$this->middleware('auth');

        //only
        //$this->middleware('auth',[ 'only'=> ['show'] ]);


        //Except
        $this->middleware('auth', ['except' => ['index', 'show', 'serch']]);
    }


    public function search(Request $request)
    {
        $request->validate([
            'from' => 'required',
            'to'   => 'required',
        ]);

        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $request->get('from'));
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $request->get('to'));

        $posts = post::whereBetween('created_at', [$from, $to])->paginate(5);
        return view('posts.index', compact('posts'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'DESC')->paginate(10);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Simple Validation
        $request->validate([
            'title' => 'bail|required|min:3',
            'body' => 'required',
        ]);


        $user = Auth::user();

        $post = new post(); //istance from post model
        $post->title = $request->input('title'); // receve title from request input and send to DB by $post instance
        $post->body = $request->input('body');
        $now = date('YmdHms');
        $post->slug = str_replace(' ', '-', strtolower($post->title)) . '-' . $now;
        $post->user_id = $user->id;
        $post->save();

        return redirect('/posts')->with('success', 'post created successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = post::where('slug', $slug)->first();

        return view('posts.show', compact('post'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        $userId = Auth::id();
        if ($post->user_id !== $userId) {
            return redirect('/posts')->with('error', 'that is not your post ');
        }

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) //from edit.blade using hidden method put
    {
        $request->validate([
            'title' => 'bail|required|min:3',
            'body' => 'required',
        ]);


        $post = POST::find($id);
        $post->title = $request->input('title'); // receve title from request input and send to DB by $post instance
        $post->body = $request->input('body');

        $userId = Auth::id();
        if ($post->user_id !== $userId) {
            return redirect('/posts')->with('error', 'that is not your post ');
        }
        //return view('posts.edit',compact('post'));

        $post->save();
        return redirect('/posts/' . $post->id . '/edit')->with('success', 'post Updated successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = POST::find($id);


        $userId = Auth::id();
        if ($post->user_id !== $userId) {
            return redirect('/posts')->with('error', 'that is not your post ');
        }

        $post->delete();
        return redirect('/posts/')->with('success', 'post Deleted successfuly');
    }
}
