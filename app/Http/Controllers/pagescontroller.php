<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class pagescontroller extends Controller
{

    public function index(){
    	$data="this is my data";
    	return view('pages.index',['data'=>$data] ) ;
    }

    public function about(){
    	return view('pages.about');
	}

	public function contact(){
		return view('pages.contact');
	}


}
