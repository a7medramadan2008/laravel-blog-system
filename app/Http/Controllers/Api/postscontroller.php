<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller; //ضيفنا ده 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Resources\PostResource;   // ضيفنا ده كمان 
use App\post;

class postscontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index','show']]);
    }

    public function index()
    {
    
        $posts = Post::orderBy('created_at', 'DESC')->paginate(10);
        
        return PostResource::collection($posts);
        
    }

    // public function create()
    // {
    //     return view('posts.create');
    // }

    public function store(Request $request)// بتستقبل من الفورم بتاعت create.blade.php
    {
        //to test the form value action 
        //     $postTitle = $request->input('title');
        //     return $postTitle;

         //Simple Validation
         $request->validate([
            'title' => 'bail|required|min:3',
            'body' => 'required',
        ]);    

        $post = new post();//istance from post model
        $post->title = $request->input('title');// receve title from request input and send to DB by $post instance
        $post->body = $request->input('body');
        $now = date('YmdHms');
        $post->slug = str_replace(' ', '-', strtolower($post->title)).'-'.$now;
        $post->user_id = auth('api')->user()->id; // to do update this in the next video
        $post->save();
        return new PostResource($post);
          }

 
    public function show($slug)// posts/{$id} بستدعيها من خلال 
    {   
         $post=post::where('slug',$slug)->firstOrfail(); 
       
        return new PostResource($post);

    }

 
    // public function edit($id)//from show.blade link 
    // {
    //     $post=Post::find($id);//متغير مخزن فيه كويري من المودل post
    //     //currunt user id
    //     $userId = Auth::id();
    //     if ($post->user_id !== $userId)
    //     {return redirect('/posts')->with('error','that is not your post ');}

    //     return view('posts.edit',compact('post'));
    // }

  
    public function update(Request $request, $id)//from edit.blade using hidden method put
    {
        $request->validate([
            'title' => 'bail|required|min:3',
            'body' => 'required',
        ]);
         
        
        $post = POST::find($id);
        $post->title = $request->input('title');// receve title from request input and send to DB by $post instance
        $post->body = $request->input('body');

        //$userId = Auth::id();
        $userId = auth('api')->user()->id;
        if ($post->user_id !== $userId)
        {return response()->json(['error' => 'This is not your post'], 401);}
       // {return redirect('/posts')->with('error','that is not your post ');}
        //return view('posts.edit',compact('post'));

        $post->save();
        return new PostResource($post);
        // return redirect('/posts/'.$post->id.'/edit')->with('success','post Updated successfuly');
    }

    public function destroy($id)
    {
        $post = POST::find($id);
        //$userId = Auth::id();
        $userId = auth('api')->user()->id;
        if ($post->user_id !== $userId)
        //{return redirect('/posts')->with('error','that is not your post ');}
        return response()->json(['error' => 'This is not your post'], 401);
        
        $post->delete();
       // return redirect('/posts/')->with('success','post Deleted successfuly');
       return response()->json(null, 204);
    }
}
