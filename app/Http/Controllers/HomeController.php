<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

         $userId = Auth::id();
         $user=User::find($userId);//quere to find user id using User model
         $posts=$user->posts;//to get the posts where is the function in user model

        return view('home',compact('posts'));
    }
    public function index1()
    {
         //currunt user id
         $userId = Auth::id();

         //FIND ALL POSTS by usr_id
         //$post=post::where('usr_id',$userId);

         $user=User::find($userId);//quere to find user id using User model
         $posts=$user->posts;//to get the posts where is the function in user model

        return view('Admin_home',compact('posts'));
    }
}
