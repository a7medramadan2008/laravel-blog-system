<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\post;

class Admincontroller extends Controller
{



     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        //$this->middleware('auth');

        //only
        //$this->middleware('auth',[ 'only'=> ['show'] ]);


        //Except
        $this->middleware('auth',[ 'except'=> ['index','show'] ]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = post::with('user')->orderBy('created_at','DESC')->paginate(10);

        return view('Admin.index',compact('posts'));



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()//العنوان بتاعها posts/create
    {
        return view('Admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'title' => 'bail|required|min:3',
            'body' => 'required',
        ]);

        $user= Auth::user();

        $post = new post();
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $now = date('YmdHms');
        $post->slug = str_replace(' ', '-', strtolower($post->title)).'-'.$now;
        $post->user_id = $user->id;
        $post->save();

        return redirect('/Admin')->with('success','post created successfuly');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post=post::where('slug',$slug)->firstOrFail();
        return view('Admin.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::find($id);
        return view('Admin.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'bail|required|min:3',
            'body' => 'required',
        ]);

        $post = POST::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->save();

        return redirect('/Admin/'.$post->id.'/edit')->with('success','post Updated successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = POST::find($id);
        $post->delete();
        return redirect('/Admin/')->with('success','post Deleted successfuly');

    }
}
