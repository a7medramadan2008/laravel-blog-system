<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Comment;
use App\post;

class CommentsController extends Controller
{

     public function __construct()
    {
         $this->middleware('auth');
    }


    public function store(Request $request, $slug) {

        $request->validate([
            'body' => 'required|min:5|max:500'
        ]);

        $post = Post::where('slug',$slug)->firstOrFail();
        $userId = Auth::id();

        $comment = new Comment();
        $comment->body = $request->body;
        $comment->post()->associate($post);
        $comment->user_id = $userId;

        $comment->save();

        return redirect()->route('posts.show', $slug)->with('success', 'Comment Added Successfully');
    }

    public function destroy($id)
    {

        $comment1 = Comment::where('post_id',$id)->firstOrFail();
        $comment1->delete();
        return redirect('/Admin/'.$comment1->post->slug)->with('success','comment Deleted successfuly');

    }

    public function edit($id)
    {
        $comment1 = Comment::where('id',$id)->first();

        return view('Comments.edit',compact('comment1'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'body' => 'required',
        ]);

        $Comment = Comment::find($id);
        // receve title from request input and send to DB by $post instance
        $Comment->body = $request->input('body');
        $Comment->save();

        return redirect('/Admin/'.$Comment->post->slug)->with('success','Comment Updated successfuly');

    }



}
