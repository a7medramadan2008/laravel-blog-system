<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//landing page
Route::get('/', 'pagescontroller@index');
//pages routs
Route::get('/about', 'pagescontroller@about')->name('aboutpage');
Route::get('/contact', 'pagescontroller@contact');
//posts route
Route::resource('posts','postscontroller');
//Admin Resources
Route::resource('Admin','Admincontroller');
//comments route
Route::post('/comments/{slug}', 'CommentsController@store')->name('comments.store');

Route::delete('comments/{id}','CommentsController@destroy');

Route::get('comments/{id}','CommentsController@edit')->name('editComment');
Route::put('comments/{id}','CommentsController@update');
//Auth route
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/Admin', 'HomeController@index1')->name('home.Admin');
Route::get('downloadExcel/{type}', 'MaatwebsiteDemoController@downloadExcel');
Route::get('/export/{type}', 'EmpController@export');
Route::post('postss','postscontroller@search')->name('posts.search');
